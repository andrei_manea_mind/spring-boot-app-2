package com.myapplication.app;

import com.myapplication.app.controller.HomeController;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class AppApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void testApp() {
		HomeController hc = new HomeController();
		String result = hc.home();
		assertEquals(result, "Reporting for duty");
	}

}
