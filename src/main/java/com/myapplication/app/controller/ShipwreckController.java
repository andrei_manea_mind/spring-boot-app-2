package com.myapplication.app.controller;

import com.myapplication.app.model.Shipwreck;
import com.myapplication.app.repository.ShipwreckRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("api/v1/")
public class ShipwreckController {
    @Autowired
    private ShipwreckRepository shipwreckRepository;

    @RequestMapping(value = "shipwrecks", method = RequestMethod.GET)
    public List<Shipwreck> list(){
        return shipwreckRepository.findAll();
    }

    @RequestMapping(value = "shipwrecks", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Shipwreck create(@RequestBody Shipwreck shipwreck) {
        return shipwreckRepository.save(shipwreck);
    }

    @RequestMapping(value = "shipwrecks/{id}", method = RequestMethod.GET)
    public Shipwreck get(@PathVariable Integer id) {
        return shipwreckRepository.getOne(id);
    }

    @RequestMapping(value = "shipwrecks/{id}", method = RequestMethod.PUT)
    public Shipwreck update(@PathVariable Integer id, @RequestBody Shipwreck shipwreck) {
        Shipwreck existingShipwreck = shipwreckRepository.findAllById(Collections.singleton(id)).get(0);
        BeanUtils.copyProperties(shipwreck, existingShipwreck);
        return shipwreckRepository.save(existingShipwreck);
    }

    @RequestMapping(value = "shipwrecks/{id}", method = RequestMethod.DELETE)
    public Integer delete(@PathVariable Integer id) {
        shipwreckRepository.deleteById(id);
        return id;
    }

}
