package com.myapplication.app.repository;

import com.myapplication.app.model.Shipwreck;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipwreckRepository extends JpaRepository<Shipwreck, Integer> {

}
